ARG DEBIAN_CONTAINER_VERSION
FROM article714/debian-based-container:$DEBIAN_CONTAINER_VERSION
LABEL maintainer="C. Guychard<christophe@article714.org>"

USER root

# Container tooling

COPY container /container

# container building

RUN /container/build.sh 
