#!/bin/bash

# Install needed packages
apt-get update
apt-get upgrade -yq
apt-get install -y python3 python3-requests openssh-server openssh-client sudo

#--
# Cleaning
apt-get -yq clean
apt-get -yq autoremove
rm -rf /var/lib/apt/lists/*
rm -f /tmp/*
# truncate logs
find /var/log -type f -exec truncate --size 0 {} \;
